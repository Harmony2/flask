from flask import Flask
from flask import render_template
from flask import request

app = Flask(__name__)



def validate(user,passwd):
    f = open("users", "r")
    users = f.readlines()
    f.close()

    for i in users:
        if i == "%s:%s\n"%(user,passwd):
            return True
    return False


@app.route('/')
def home():
    return render_template("home.html")

@app.route('/login', methods=["GET","POST"])
def login():
    if request.method == "POST":
        user = request.form.get('user')
        passwd = request.form.get('pass')
    else:
        return render_template("unauth.html")

    if validate(user, passwd):
        f = open("todo/%s"%user, "r")
        todo = f.readlines()
        f.close()
        return render_template("index.html", user=user, todo=todo)
    return render_template("home.html", error="Wrong username or password")

@app.route("/add", methods=["GET","POST"])
def additem():
    if request.method == "POST":
        user = request.form.get("user")
        item = request.form.get("item")
        if (" " in item):
            return render_template("index.html", error="Use one word only")
        f = open("todo/%s"%user,"a")
        f.write("%s\n"%item)
        f.close()
        f = open("todo/%s"%user, "r")
        todo = f.readlines()
        f.close()
        return render_template("index.html", user=user, todo=todo)
    else:
        return render_template("unauth.html")

@app.route("/rm", methods=["GET","POST"])
def rmitem():
    if request.method == "POST":
        user = request.form.get("user")
        remove = request.form.get("remove")

        f = open("todo/%s"%user,"r")
        l = f.readlines()
        f.close()

        f = open("todo/%s"%user,"w")
        for i in l:
            print(i + remove)
            if i != (remove + "\n"):
                f.write(i)
        f.close()


        f = open("todo/%s"%user, "r")
        todo = f.readlines()
        f.close()
        return render_template("index.html", user=user, todo=todo)
    else:
        return render_template("unauth.html")

@app.route('/register')
def register():
    return render_template("register.html")

@app.route('/reg', methods=["GET", "POST"])
def reg():
    if request.method == "POST":
        user = request.form.get("user")
        passwd1 = request.form.get("password1")
        passwd2 = request.form.get("password2")
    else:
        return render_template("register.html", error="Wrong Method.")
    
    f = open("users","r")
    r = f.readlines()
    f.close()

    for i in r:
        if "%s:"%user in i:
            return render_template("register.html", error="User already taken")

    if passwd1 != passwd2:
        return render_template("register.html", error="Different password fields")
    else:
        f = open("users","a")
        f.write("%s:%s\n"%(user,passwd1))
        f.close()
        f = open("todo/%s"%user, "a")
        f.close()
        return render_template("home.html")

if __name__ == '__main__':
    app.run(debug=False,host='0.0.0.0')
FROM python:3.7-alpine

WORKDIR /app/

RUN pip3 install Flask
COPY /code/app.py /app/app.py
COPY /code/templates/ /app/templates/
COPY /code/static/ /app/static/
RUN mkdir /app/todo/
RUN touch /app/users

ENTRYPOINT ["python3", "app.py"]